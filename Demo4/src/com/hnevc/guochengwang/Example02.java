package com.hnevc.guochengwang;

/**
 * 【super关键字，final关键字】
 * super访问父类当中的成员
 * final 一经修饰不得修饰
 */
public class Example02 {
    public static void main(String[] args) {
        Dog1 dog1 = new Dog1();
        dog1.shout();
        dog1.name = "";
        dog1.printName();

    }
}

class Animal1 {
    final String name;

    void shout() {
        System.out.println("动物发出叫声音");

    }

    public Animal1(String name) {
        this.name = name;
    }
}

class Dog1 extends Animal1 {
    String name = "类";

    public Dog1() {
        super("边牧");
    }


    @Override
    void shout() {
        super.shout();
    }

    void printName() {
        final int age = 0;
        System.out.println("name"+ super.name);
    }


}

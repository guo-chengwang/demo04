package com.hnevc.guochengwang;

import sun.awt.geom.AreaOp;

/**
 *
 * 继承
 * 子类自动拥有父类的所有可继承的属性和方法
 *extends关键字
 * 【重写
 *
 */
public class Example01 {

    public static void main(String[] args) {
       Dog dog = new Dog();//实例化Dog子类
        dog.name = "图图";//子类去访问父类当中的属性
        dog.shout();//子类去访问父类当中的方法
        dog.printName();
        }

    }


//父类 Animal
class Animal{

    String name; //名字属性
    //动物叫的方法
    void shout(){
        System.out.println("动物发出的叫声");
    }
}
//子类Dog
class Dog extends Animal{
    public void printName(){
        System.out.println("name=" +name);
    }
//重写父类当中shout方法
    @Override
    void shout() {
        System.out.println("汪汪汪~~~");
    }
}
package com.hnevc.guochengwang;

public class Example04 {
    public static void main(String[] args) {
     BigDataClassOne bigDataClassOne = new BigDataClassOne();
     bigDataClassOne.eat();
     bigDataClassOne.playGame();
     bigDataClassOne.sleep();
    }
}
interface Student{
    String STUDENT_BEHAVIOR ="学生的行为";

    void sleep();
    void playGame();
    void eat();

}
class BigDataClassOne implements Student{

    @Override
    public void sleep() {
       System.out.println("大数据一班全班在睡觉");
    }

    @Override
    public void playGame() {
       System.out.println("大数据一班全班在打游戏");
    }

    @Override
    public void eat() {
       System.out.println("大数据一班全班在吃");
    }
}
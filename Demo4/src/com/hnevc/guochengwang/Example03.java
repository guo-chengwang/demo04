package com.hnevc.guochengwang;

/**
 *
 * 【抽象方法，抽象类】
 * 用abstract关键字进行修饰的类为抽象类
 * 一个抽象类可以没有抽象方法
 * 有抽象方法该类为抽象类
 * 抽象方法没有方法体，抽象类不能够被实例化（new）
 *
 */
public class Example03 {
    public static void main(String[] args) {
     SmCar smCar =new SmCar();
     smCar.run();
    }
}
abstract       class Car {
    abstract void run();
}
class SmCar extends Car{
    @Override
    void run() {
        System.out.println("车跑起来了");
    }
}